<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Low AUS Decile</label>
    <protected>false</protected>
    <values>
        <field>ApplyDiscounttoSalesPrice__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">S01</value>
    </values>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">AU</value>
    </values>
    <values>
        <field>DecileHighRange__c</field>
        <value xsi:type="xsd:double">999.0</value>
    </values>
    <values>
        <field>DecileLowRange__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>DiscountDescription__c</field>
        <value xsi:type="xsd:string">School Type Docile</value>
    </values>
    <values>
        <field>DiscountPercentage__c</field>
        <value xsi:type="xsd:double">37.5</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LineItemDescriptionVisible__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Territory__c</field>
        <value xsi:type="xsd:string">APAC</value>
    </values>
</CustomMetadata>
