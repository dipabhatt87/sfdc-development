<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Public</label>
    <protected>false</protected>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">S01</value>
    </values>
    <values>
        <field>Discount__c</field>
        <value xsi:type="xsd:double">29.18</value>
    </values>
    <values>
        <field>School_Category__c</field>
        <value xsi:type="xsd:string">Government</value>
    </values>
</CustomMetadata>
