<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>APAC AU WholeSchool Spellodrome Country</label>
    <protected>false</protected>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">C05</value>
    </values>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">AU</value>
    </values>
    <values>
        <field>Discount_Type__c</field>
        <value xsi:type="xsd:string">Country</value>
    </values>
    <values>
        <field>Pricebook__c</field>
        <value xsi:type="xsd:string">Whole_School_Price</value>
    </values>
    <values>
        <field>Product_Family__c</field>
        <value xsi:type="xsd:string">Spellodrome</value>
    </values>
    <values>
        <field>Territory__c</field>
        <value xsi:type="xsd:string">APAC</value>
    </values>
</CustomMetadata>
