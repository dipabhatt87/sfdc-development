<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Mathseeds_Tiered3</label>
    <protected>false</protected>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">Q06</value>
    </values>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">AU</value>
    </values>
    <values>
        <field>Discount__c</field>
        <value xsi:type="xsd:double">40.0</value>
    </values>
    <values>
        <field>High_Tiered__c</field>
        <value xsi:type="xsd:double">225.0</value>
    </values>
    <values>
        <field>Low_Tiered__c</field>
        <value xsi:type="xsd:double">151.0</value>
    </values>
    <values>
        <field>Territory__c</field>
        <value xsi:type="xsd:string">APAC</value>
    </values>
</CustomMetadata>
