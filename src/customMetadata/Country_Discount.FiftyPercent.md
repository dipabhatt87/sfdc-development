<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>FiftyPercent</label>
    <protected>false</protected>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">C03</value>
    </values>
    <values>
        <field>Discount__c</field>
        <value xsi:type="xsd:double">50.58</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
