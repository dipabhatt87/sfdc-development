<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CAN_Purchase_Silver_BM_SDR</label>
    <protected>false</protected>
    <values>
        <field>Account_Team_Role__c</field>
        <value xsi:type="xsd:string">SDR</value>
    </values>
    <values>
        <field>Contact_Job_Function__c</field>
        <value xsi:type="xsd:string">Business_Manager</value>
    </values>
    <values>
        <field>Create_Task__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enquiry_Type__c</field>
        <value xsi:type="xsd:string">Purchase</value>
    </values>
    <values>
        <field>Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Opportunity_Stage__c</field>
        <value xsi:type="xsd:string">Verbal_Agreement</value>
    </values>
    <values>
        <field>Process_Type__c</field>
        <value xsi:type="xsd:string">Semi_Automated</value>
    </values>
    <values>
        <field>School_Classification__c</field>
        <value xsi:type="xsd:string">Silver</value>
    </values>
    <values>
        <field>Send_email__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Territory__c</field>
        <value xsi:type="xsd:string">Canada</value>
    </values>
</CustomMetadata>
