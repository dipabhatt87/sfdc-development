<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TwoYears Upfront</label>
    <protected>false</protected>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">M01</value>
    </values>
    <values>
        <field>Discount__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Pay_Upfront__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Term__c</field>
        <value xsi:type="xsd:string">2</value>
    </values>
</CustomMetadata>
