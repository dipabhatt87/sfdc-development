/**
 * @author: Baskar Venugopal
 * @Description: This batch is used to send documents for ESign thru Docverify API.
 * @Test Class:  GenerateQuoteEsign_Test 
 * @History: 
	08/08/2017 created.
 */
global class SendEsignBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Schedulable{
    
    private id quoteId;
    private id contactId;
    public SendEsignBatch(Id quoteId,id contactId){
        this.quoteId=quoteId;
        this.contactId= contactId;
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'Select id,Document,Name,Quote.Description from QuoteDocument where QuoteId = \''+ quoteId + '\'';
        system.debug('query##'+ query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<QuoteDocument> scope)
    {
        system.debug(scope[0] + '###'+quoteId );
        GenerateQuoteAndEsignature.addNewDocAndEsign(scope[0],quoteId,contactId);
        
    }  
    global void finish(Database.BatchableContext BC)
    {
    }
    
    //Schedulable interface
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new SendEsignBatch(quoteId,contactId), 10);
    }
}