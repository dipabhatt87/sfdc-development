public class OpportunityBeforeInsertTriggerHandler extends TriggerHandlerBase { 
    public override void mainEntry(TriggerParameters tp) {
       process(tp.newList);
    }
  
    private static id clusterAccountRecodTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cluster').getRecordTypeId();
    private static id clusterAccountOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Cluster Opportunity').getRecordTypeId();
    
    private static void process(List<Opportunity>opportunityList) {     
   
       updateRecordTypeIfAccountIsCluster(opportunityList);
    }
    
    public static List<Opportunity> updateRecordTypeIfAccountIsCluster(List<Opportunity>opportunityList)
    {
        List<id> accountsToCheck = new List<id>();
        List<Opportunity> resultList = new List<Opportunity>();
        for(Opportunity oppo :opportunityList)
        {
            accountsToCheck.add(oppo.accountid);
        }
        if(!accountsToCheck.isEmpty())
        {
            map<id,Account> accountIdToRecTypeId = new Map<ID, Account>
            ([SELECT Id, Name,recordtypeid FROM Account where id in :accountsToCheck]);  
            for(Opportunity oppo :opportunityList)
            {
                id recId = accountIdToRecTypeId.get(oppo.accountid).recordtypeid;
                if(accountIdToRecTypeId.get(oppo.accountid) !=null && recId.equals(clusterAccountRecodTypeId) )
                {
                    oppo.recordtypeid = clusterAccountOpportunityRecordTypeId;
                    resultList.add(oppo);
                }
            }  
        }
        return resultList;
    }


}