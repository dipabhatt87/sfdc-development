/**
 * @author- Amit Shirude
 * @Description This class schedular to schedul UpdateLicenceStatus batch.
 * @Test Class:    LicenceUpdateSchedular_Test
 * @History: 
	22/04/2016 created.
 */

global class LicenceUpdateSchedular implements Schedulable{    
    
    global void execute(SchedulableContext sc) {  
        Integer checkLicenceCount = Database.countQuery('select count() from Asset where Status IN (\'Expired\',\'Provisioned\') and UsageEndDate < Today');
        String logInfo = 'Start LicenceUpdateSchedular, will check ' + checkLicenceCount + ' Assets';       
        System.debug(logInfo);
        ExceptionLogData.InsertInfolog(null, Logginglevel.INFO, 'LicenceUpdateSchedular', false, false, logInfo, null);

        String licenceQuery = 'select id,UsageEndDate,Status from Asset where Status IN (\'Expired\',\'Provisioned\') and UsageEndDate < Today';
        Database.executeBatch(new StatelessBatchExecutor(new UpdateLicenceStatus(), licenceQuery, null), 200);

        logInfo = 'LicenceUpdateSchedular executed completely';
        System.debug(logInfo);
        ExceptionLogData.InsertInfolog(null, Logginglevel.INFO, 'LicenceUpdateSchedular', false, false, logInfo, null);
    }
}