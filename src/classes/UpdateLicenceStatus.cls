/**
 * @author: Amit Shirude
 * @Description: This class implement the StatelessBatchExecutor to Update Licence Status.
 * @Test Class:    UpdateLicenceStatus_Test
 * @History: 
	21/04/2016 created.
 */
public class UpdateLicenceStatus implements StatelessBatchExecutor.CustomBatchable{

    static Integer licenceCount = 0;
    static Integer updateLicenceCount = 0;
    static Integer updateSuccessCount = 0;
    static Integer updateFailCount = 0;

    public void execute(List<Asset> scope){           
        licenceCount = scope.size();
        updateLicenceCount = 0;
        updateSuccessCount = 0;	
        updateFailCount = 0;

        updateAssetstoExpired(scope);	
    }   
   
	/**
	 * @author: Amit Shirude
	 * @Description: Method to update Licences to Expired.
	 * @Param : Asset List to update.	 
	 * @History: 
		21/04/2016 created.
	 */
    public void updateAssetstoExpired(List<Asset> AssetList){
        
        try{           
            List<Asset> needUpdateAssetList = new List<Asset>();
            for(Asset aset : AssetList){
                if(aset.Status == 'Provisioned' ){
                    if(aset.UsageEndDate < System.today()){
                        aset.Status = 'Expired';
                        needUpdateAssetList.add(aset);
                    }
                }
                else if(aset.Status == 'Expired'){                    
                    if(aset.UsageEndDate < System.Today()-90){                        
                        aset.Status = 'Lost';
                        needUpdateAssetList.add(aset);
                    }
                }
            }
            updateLicenceCount = needUpdateAssetList.size();

            if (updateLicenceCount > 0) {
                String logInfo = 'Start update Assets status, totally ' + updateLicenceCount + ' Assets will be updated.';
                System.debug(logInfo);
                ExceptionLogData.InsertInfolog(null, Logginglevel.INFO, 'UpdateLicenceStatus', false, false, logInfo, null);

                Database.SaveResult[] updateResult = Database.update(needUpdateAssetList, false);
                
                logUpdateResult(needUpdateAssetList, updateResult);

                logInfo = 'Finish update Assets status, totally ' + updateLicenceCount + ' Assets were updated, ' + 
                          updateSuccessCount + ' Assets update status success, ' + updateFailCount + ' Assets update status failed.';
                System.debug(logInfo);  
                ExceptionLogData.InsertInfolog(null, Logginglevel.INFO, 'UpdateLicenceStatus', false, false, logInfo, null);
            }
        }catch(Exception ex){
            System.debug('Exception - ' + ex);
            ExceptionLogData.InsertExceptionlog(null, Logginglevel.ERROR, 'UpdateLicenceStatus', ex, true, false, 'Exception - ' + ex, null);
        }
    }

    @TestVisible
    private void logUpdateResult(List<Asset> needUpdateAssetList, Database.SaveResult[] updateResult)
    {
        String logInfo;

        for (Integer i = 0; i < updateResult.size(); i++) {
            Database.SaveResult result = updateResult[i];
            Asset asset = needUpdateAssetList[i];

            if (result.isSuccess()) {
                updateSuccessCount++;

                logInfo = 'Successfully updated asset status. Asset Id:' + result.getId();
                System.debug(logInfo);
                ExceptionLogData.InsertInfolog(result.getId(), Logginglevel.INFO, 'UpdateLicenceStatus', false, false, logInfo, null);
            }
            else {
                updateFailCount++;

                System.debug('Update asset status failed. Asset Id:' + asset.Id + '. The following error(s) occurred:');
                for (Database.Error e : result.getErrors()){ 
                    logInfo = e.getStatusCode() + ':' + e.getMessage();
                    if (e.getFields().size() > 0) {
                        logInfo += '; Fields that affected this error:';
                        for (String field : e.getFields()) {
                            logInfo += field + ',';
                        }
                        logInfo = logInfo.removeEnd(',');
                    }

                    System.debug(logInfo); 
                    ExceptionLogData.InsertExceptionlog(asset.Id, Logginglevel.ERROR, 'UpdateLicenceStatus', 
                        new CustomException(logInfo), true, false, logInfo, null);
                }
            }
        }
    }
    
    public void finish(){           
        System.debug('UpdateLicenceStatus finished');	
    }
}