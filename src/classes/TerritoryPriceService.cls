@RestResource(urlMapping='/territory/price/*')
 global with sharing class TerritoryPriceService {


    public  static Map<String, String> ToLowerCase(Map<String, String> parameters){
      Map<String, String> params = new    Map<String, String>();
      // alternatively ApexPages.currentPage().getParameters()
      for (String exactKey : parameters.keySet())
      {
          if (!params.containsKey(exactKey)) // case insensitive
          {
              params.put(exactKey.toLowerCase(), parameters.get(exactKey));
          }
      }

      return params;
    }


    @httpGet
    global static void  Get(){
      try{
          RestResponse res = RestContext.response;
          if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
          }

          //Get Parameters
          Map<String, String> parameters =ToLowerCase( RestContext.request.params);
          String territoryName = parameters.get('territory');        
          String numberoflicences =parameters.get('numberoflicences');
          string campaignIdParam =parameters.get('campaignid');
          String productfamily =parameters.get('productfamily');
          String country =parameters.get('country');
          String terms =parameters.get('term');

          //Validation

          String responseBody = new MCServicesValidator().validateTerritoryServiceInputParameters(numberoflicences, campaignIdParam,productfamily,country, territoryName,terms); 
          if(responseBody != null){
            res.statusCode = 400;
            res.responseBody=Blob.valueOf(responseBody);
          return;
          }
        
          //Map<String,TerritoriesForNewPricing__c> TerritoriesForNewPricing = TerritoriesForNewPricing__c.getAll();
          //if(!TerritoriesForNewPricing.containsKey(territoryName)){
          List<Territory__mdt> territories = [Select Name__c from Territory__mdt where Name__c =:territoryName];
          List<ISOMapping__c> isoMappings = [select Id from ISOMapping__c where Territory__c = :territoryName and (ISO2__c = :country OR ISO3__c= :country)];


          PriceModel price =new PriceModel();
          if(territories.size() == 0 || isoMappings.size() == 0){
           res.statusCode = 200;
           price.ispriceavailable = false;
           res.responseBody=Blob.valueOf(JSON.serialize(price)); 
           return;
          }
           

          // PriceModel price =new PriceModel();
          List<Marketing_Cloud_Default_Pricebook__mdt> pricebookListForMC = [Select Pricebook__r.Name__c,Product_family__r.Name__c,Country__r.Code__c,Country__r.Tax_Name__c,
                                                                              Country__r.Currency__c, 
                                                                              Territory__r.Name__c  from Marketing_Cloud_Default_Pricebook__mdt
                                                                              Where Product_family__r.Name__c = :productfamily and 
                                                                              (Country__r.Code__c = :country OR Country__r.Code__c  = 'All') And                   
                                                                              Territory__r.Name__c = :territoryName];
           Marketing_Cloud_Default_Pricebook__mdt priceBook = new Marketing_Cloud_Default_Pricebook__mdt();
          if(pricebookListForMC.size() > 1){
              for(Marketing_Cloud_Default_Pricebook__mdt MCpriceBook : pricebookListForMC){
                if(MCpriceBook.Country__r.Code__c != 'All') {
                      priceBook = MCpriceBook;
                }
              }
          }else{
            priceBook = pricebookListForMC[0];
          }

          System.debug('priceBook.Country__r.Code__c:' + priceBook.Country__r.Code__c + ' ,priceBook.Country__r.Currency__c:' + priceBook.Country__r.Currency__c);   
          List<PricebookEntry> priceBookEntry = [Select Id,Name, IsActive,Product2Id,PriceBook2Id,Pricebook2.Name from PriceBookEntry 
                  where 
                    product2.Family = :productfamily And 
                    Pricebook2.Name = :priceBook.Pricebook__r.Name__c and 
                    CurrencyIsoCode = :priceBook.Country__r.Currency__c
                    And  IsActive = true];

          List<ACTNSPC__Price_Level_Entry__c> pricelevelEntries = [SELECT Id, ACTNSPC__Currency__c, ACTNSPC__Price_Level__c, ACTNSPC__External_Id__c,   ACTNSPC__Tier_1_Price__c, ACTNSPC__Tier_2_Price__c,ACTNSPC__Product__c  
                                          FROM ACTNSPC__Price_Level_Entry__c
                                          where ACTNSPC__Price_Level__R.Name = :priceBook.Pricebook__r.Name__c
                                          and ACTNSPC__Currency__R.Name      = :priceBook.Country__r.Currency__c];

          System.debug('pricelevelEntries.size():' + pricelevelEntries.size());
          if(pricelevelEntries == null || pricelevelEntries.size()==0 )
          {
            Throw new ValidationException('PriceLevelEntry is null');
          }
          PriceCalculator priceCalculator = new PriceCalculator();
          //Function call to get tired price
          
          priceCalculator.SetDiscountTiers(territoryName, country, priceBook.Pricebook__r.Name__c, productFamily);
          boolean isPaymentUpfront =true;
          AdvancedPriceModel advPriceModel = priceCalculator.GetPrice(priceLevelEntries[0].ACTNSPC__Tier_1_Price__c,  integer.valueOf(numberoflicences), integer.valueOf(terms), isPaymentUpfront, '-1', '');

           System.debug('advPriceModel:' + advPriceModel);
          price.UnitPrice = advPriceModel.UnitPriceIncludeTax;
          price.Tax       = advPriceModel.TaxAmount;
          price.currencyCode = priceBook.Country__r.Currency__c;
          price.Subtotal  = advPriceModel.FinalAmountExcludeTax ;
          price.TotalAmount  = advPriceModel.FinalAmountIncludeTax;
          price.DicountedPriceIncludedTax = advPriceModel.discountAmount;
          price.Taxname   = priceBook.Country__r.Tax_Name__c;
          price.ispriceavailable = true;
          price.pricebookid= priceBookEntry[0].Pricebook2id;
          price.productId= priceBookEntry[0].Product2Id;
          price.monthlyUnitPrice = advPriceModel.monthlyUnitPrice;
          res.responseBody = Blob.valueOf(JSON.serialize(price));
          res.statusCode = 200;
       
      }catch(Exception ex){
            system.debug('Validation Exception: '+ ex.getMessage());
            ExceptionLogData.InsertExceptionlog(null, LoggingLevel.ERROR, 'SchoolPriceService', ex, true, false, null, null);
      }       
     
  }
}