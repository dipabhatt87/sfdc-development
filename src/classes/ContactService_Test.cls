@isTest
private class ContactService_Test
{

	@testSetup static void setup()
    {
    	FakeObjectFactory.testclasssetup();
    }
	
	@isTest
	static void testContactService()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,null, contact.FirstName, contact.LastName,'Accounts/Finance', 'Account Payble', contact.Email, productFamily,'1234');


		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

	@isTest
	static void testContactServiceWithAllFields()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,contact.Id, contact.FirstName, contact.LastName,'Accounts/Finance', 'Account Payble', contact.Email, productFamily,'1234');
		 System.assertEquals(res.statusCode, 200);
		  System.assertEquals(res.responseBody.toString(), ContactId);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

	@isTest
	static void testContactServiceWithLastNameNull()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,null, contact.FirstName, null,'Accounts/Finance', 'Account Payble', contact.Email, productFamily,'1234');
		 System.assertEquals(res.responseBody.toString(), 'lastName needs to be provided.');
        System.assertEquals(res.statusCode, 400);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

	@isTest
	static void testContactServiceWithLastFirstNameNull()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,null, null, contact.LastName,'Accounts/Finance', 'Account Payble', contact.Email, productFamily,'1234');
		 System.assertEquals(res.responseBody.toString(), 'firstName needs to be provided.');
        System.assertEquals(res.statusCode, 400);


		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

		@isTest
	static void testContactServiceWithEmailNull()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,null, contact.FirstName, contact.LastName,'Accounts/Finance', 'Account Payble', null, productFamily,'1234');
		 
		  System.assertEquals(res.responseBody.toString(), 'email needs to be provided.');
        System.assertEquals(res.statusCode, 400);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

		@isTest
	static void testContactServiceWithJobNull()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,null, contact.FirstName, contact.LastName, null, 'Account Payble', contact.Email,productFamily,'1234');
		  System.assertEquals(res.responseBody.toString(), 'job needs to be provided.');
        System.assertEquals(res.statusCode, 400);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

		@isTest
	static void testContactServiceWithRoleNull()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,null, contact.FirstName, contact.LastName, 'Accounts/Finance', null,contact.Email, productFamily,'1234');
		 System.assertEquals(res.statusCode, 200);
		  System.assertEquals(res.responseBody.toString(), ContactId);
		 Contact_Roles__c contactRole = [Select Id,Name,Role__c from Contact_Roles__c where Contact__c = :ContactId];
		  System.assertEquals(contactRole.Role__c, 'Opportunity Contact');
        
		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

	/*@isTest
	static void testContactServiceWithProductFamilyNull()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;
		Contact con = [Select Id,Name,AccountId from contact where Id = :contact.Id];	
		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(con.AccountId,con.Id, contact.FirstName, contact.LastName, 'Account/Finance', 'Account Payble',contact.Email, null,'1234');
		 

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}*/
	@isTest
	static void testContactServiceWithContactIdNummNull()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,null, contact.FirstName, contact.LastName, 'Accounts/Finance', 'Account Payble',contact.Email, productFamily,'1234');
		 System.assertEquals(res.statusCode, 200);
		 System.assertEquals(res.responseBody.toString(), ContactId);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

	@isTest
	static void testContactServiceForNewContact()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,null, 'new contact', 'new lastName', 'Accounts/Finance', 'Account Payble','abc@xyz.com', productFamily,'1234');
		 System.assertEquals(res.statusCode, 200);
		 System.assertEquals(res.responseBody.toString(), ContactId);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

	@isTest
	static void testContactServiceForOldContactRole()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;

        Contact_Roles__c conRole = new Contact_Roles__c();
        conRole.Role__c  = 'Subscription Coordinator';
        conRole.Role_Status__c = 'Current';
        conRole.MInfluencer__c = true;
        conRole.Contact__c = contact.Id;
        conRole.Account__c = newAccount.Id;
        insert conRole;

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		
		 Id ContactId = ContactService.dopost(newAccount.Id,contact.ID, contact.FirstName, contact.LastName, 'Accounts/Finance', 'Account Payble',contact.Email, productFamily,'1234');
		 System.assertEquals(res.statusCode, 200);
		 System.assertEquals(res.responseBody.toString(), ContactId);
		 
		 Id ContactId2 = ContactService.dopost(newAccount.Id,contact.ID, contact.FirstName, contact.LastName, 'Accounts/Finance', 'Account Payble',contact.Email, 'Spellodrome','1234');
		 System.assertEquals(res.statusCode, 200);
		 System.assertEquals(res.responseBody.toString(), ContactId2);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

	@isTest
	static void testContactServiceForInactiveContact()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        contact.Status__c = 'Inactive';
        insert contact;

        Contact_Roles__c conRole = new Contact_Roles__c();
        conRole.Role__c  = 'Subscription Coordinator';
        conRole.Role_Status__c = 'Current';
        conRole.MInfluencer__c = false;
        conRole.Contact__c = contact.Id;
        conRole.Account__c = newAccount.Id;
       // insert conRole;

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,contact.ID, contact.FirstName, contact.LastName, 'Accounts/Finance', 'Account Payble',contact.Email, productFamily,'1234');
		 System.assertEquals(res.statusCode, 200);
		 System.assertEquals(res.responseBody.toString(), ContactId);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}

	@isTest
	static void testContactServiceForOldContactRoleToSetInfluencers()
	{
		String productFamily = 'Mathletics';
		   Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;

 		Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;

        Contact_Roles__c conRole = new Contact_Roles__c();
        conRole.Role__c  = 'Subscription Coordinator';
        conRole.Role_Status__c = 'Current';
        conRole.MInfluencer__c = false;
        conRole.SInfluencer__c = false;
        conRole.MSInfluencer__c = true;
        conRole.Contact__c = contact.Id;
        conRole.Account__c = newAccount.Id;
        insert conRole;

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/contact';  //Request URL
		req.httpMethod = 'POST';//HTTP Request Type
		RestContext.request = req;
		RestContext.response= res;

		ContactService s = new ContactService ();
		 Id ContactId = ContactService.dopost(newAccount.Id,contact.ID, contact.FirstName, contact.LastName, 'Accounts/Finance', 'Account Payble',contact.Email, productFamily,'1234');
		 System.assertEquals(res.statusCode, 200);
		 System.assertEquals(res.responseBody.toString(), ContactId);
		 Id ContactId2 = ContactService.dopost(newAccount.Id,contact.ID, contact.FirstName, contact.LastName, 'Accounts/Finance', 'Account Payble',contact.Email, 'Spellodrome','1234');
		 System.assertEquals(res.statusCode, 200);
		 System.assertEquals(res.responseBody.toString(), ContactId2);

		//system.assert(res.get('isSuccess')==true);//Similarly assert rest as well

	}






}