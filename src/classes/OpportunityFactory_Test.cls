@isTest
private class OpportunityFactory_Test
{
	 @testSetup static void setup()
    {
        FakeObjectFactory.testclasssetup();

    }
	@isTest
	static void Calling_OpportunityFactory_CreateOpportuntityLineItem_OneOpportunityLineItemShouldBeCreated()
	{
		
		Id pricebookEntryId= TestUtility.getFakeId(PriceBookEntry.SObjectType);

		OpportunityLineItem opportunitylineItem = OpportunityFactory.Create(1, true, system.today(), system.today().addDays(30), 1, pricebookEntryId);

		System.assertNotEquals(null, opportunitylineItem);

	}



	@isTest
	static void Calling_OpportunityFactory_CloneOpportuntityLineItem_OneOpportunityLineItemShouldBeCreated()
	{
		
		Id pricebookEntryId= TestUtility.getFakeId(PriceBookEntry.SObjectType);

		OpportunityLineItem opportunityLineItem = OpportunityFactory.Create(1, true, system.today(), system.today().addDays(30), 1, pricebookEntryId);

		OpportunityLineItem clonedOpportunitylineItem = OpportunityFactory.CloneOpportunityLineItem(opportunityLineItem, system.today(),  1, true, 1) ;

		System.assertNotEquals(null, clonedOpportunitylineItem);

	}

	@isTest
	static void Calling_OpportunityFactory_CreateOpportuntity_OneOpportunityShouldBeCreated()
	{
		
		Product2 product = FakeObjectFactory.GetProduct('Mathletics');
        insert product;

        Id pricebookId = Test.getStandardPricebookId();  
      
		Id accountId= TestUtility.getFakeId(Account.SObjectType);

        Opportunity   opportunity  =  OpportunityFactory.Create('Up Sell', 'Standard Opportunity', pricebookId, product.Family, accountId, 'AUD','Quote') ; 		

		System.assertNotEquals(null, opportunity);

	}

	

	@isTest
	static void Calling_OpportunityFactory_CloneOpportuntity_OneOpportunityShouldBeCreated()
	{
		
		Product2 product = FakeObjectFactory.GetProduct('Mathletics');
        insert product;

        Id pricebookId = Test.getStandardPricebookId();  

        Id standardOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Standard Opportunity').getRecordTypeId();
      
		
		Opportunity opportunity = FakeObjectFactory.GetStandardOpportunity();

        Opportunity   clonedOpportunity  =  OpportunityFactory.CloneOpportunity(opportunity, pricebookId, product.Family, standardOpportunityRecordTypeId) ;
		
		System.assertNotEquals(null, clonedOpportunity);

	}
    
    @isTest
    static void Calling_OpportunityFactory_createQuote(){
        
        Account account  = FakeObjectFactory.GetSchoolAccount();
        account.School_Classification__c='Bronze';
        account.Territory__c='APAC';
        insert account; 
        
        Contact contact=FakeObjectFactory.GetContact();
        TestUtility.AssignAccountToContact(account, contact);    
        insert contact;

        Product2 product = FakeObjectFactory.GetProduct('Mathletics');
        insert product;
        
        Id pricebookId = Test.getStandardPricebookId();  
        
        Opportunity opportunity = FakeObjectFactory.GetStandardOpportunity();
        opportunity.AccountId = account.id;
        insert opportunity;
        
        
		PricebookEntry pricebookEntry = FakeObjectFactory.GetPriceBook(product, 1);   
        insert pricebookEntry;  

        
        Quote quote=OpportunityFactory.createQuote('Quotename', opportunity.Id, pricebookId);
        System.debug('THis is to check quote before inserting'+ quote);
        insert quote;
        System.assertNotEquals(null, quote);
        
        QuoteLineItem quoteLineItem= OpportunityFactory.createQuoteLineItem( product.id,quote.Id,pricebookEntry.id,10,5);
        insert quoteLineItem;
        System.assertNotEquals(null, quoteLineItem);
        
        OpportunityContacts__c oppCont= OpportunityFactory.createOpportunityContact(account.id,contact.id, opportunity.id, 'Opportunity Contact');
        insert oppCont;
        
        System.assertNotEquals(null, oppCont);
    }
}