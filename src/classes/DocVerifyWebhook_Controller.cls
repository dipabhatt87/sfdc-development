/**
* @author: Baskar Venugopal
* @Description: This is controller class for Docverify Webhook. This class will received status update from Docverify.
* @Test Class:  DocverifyController_Test
* @History: 
07/07/2017 created.
*/
public without sharing class DocVerifyWebhook_Controller {
    
    /**
    * @author: Baskar Venugopal	
    * @Description: Method to fetch all the Docverify status update parameters.	 
    * @History: 
    07/07/2017 created.
    */
    public PageReference  updateStatus(){
        Map<String, String> formData = ApexPages.currentPage().getParameters();
        string params='';
        string docId=formData.get('docId');
        string signer=formData.get('signer');
        string status= formData.get('status');
        string statusdate =formData.get('statusdate');
        string POnumber=formData.get('PONumber');
        for (string key : formData.keySet()){
            if (formData.get(key) != null){
                params += key + ' = ' + formData.get(key) + '\n';
            }
        }
        
        try{
            
            List<docverifyesign__DocVerify_Agreement__c> eSignList=[Select id,docverifyesign__Document_ID__c,docverifyesign__Status__c,docverifyesign__Quote__r.OpportunityId from  
                                                                    docverifyesign__DocVerify_Agreement__c where  docverifyesign__Document_ID__c=: docId];
            if(eSignList != null && eSignList.size() >0){
                docverifyesign__DocVerify_Agreement__c eSign=eSignList[0];
                eSign.docverifyesign__Status__c =status;
                if(status =='signed') {eSign.docverifyesign__Date_Signed__c = system.Date.today();}
                update eSign;
                
                List<Opportunity> opportunityList=[Select id,StageName,Won_Lost_Reason__c ,
                                                   (Select Id,Name,SubscriptionEndDate__c,SubscriptionStartDate__c From OpportunityLineItems)
                                                   from Opportunity where 
                                                   id=: eSignList[0].docverifyesign__Quote__r.OpportunityId 
                                                   and Is_Automatically_Created__c= true ];
                
                system.debug('opportunityList##'+opportunityList);
                if(opportunityList != null && opportunityList.size() >0){
                    Opportunity oppTobeUpdated=opportunityList[0];
                    List<OpportunityLineItem> oppLineItems=oppTobeUpdated.OpportunityLineItems;
                    integer oppSubscriptionEnd;
                    date subscriptionEndDate;
                    for(OpportunityLineItem oppLineItem : oppLineItems){
                        subscriptionEndDate=oppLineItem.SubscriptionEndDate__c;
                        oppSubscriptionEnd= subscriptionEndDate.year() - system.Date.today().year();
                        if(status =='signed' || status =='completed'){
                            oppLineItem.SubscriptionStartDate__c =system.Date.today();
                            oppLineItem.SubscriptionEndDate__c = system.date.today().addYears(oppSubscriptionEnd);
                            oppLineItem.SubscriptionEndDate__c =oppLineItem.SubscriptionEndDate__c.addDays(-1);
                        }
                         system.debug('oppLineItem##'+ oppLineItem);
                    }
                   
                    update oppLineItems;
                    DocverifyEsignForQuotes__c quoteSettings=DocverifyEsignForQuotes__c.getValues('GenerateQuote');
                    
                    if(status =='signed' || status =='completed'){
                        oppTobeUpdated.Is_Docverify_Signed_Or_Completed__c=true;
                        oppTobeUpdated.StageName =quoteSettings.OppStage_Doc_Signed__c; //Verbal Agreement;
                        oppTobeUpdated.Won_Lost_Reason__c='Best product !';
                    } // Keep this stage in Custom Settings
                   
                    if(status =='declined'){
                        oppTobeUpdated.StageName =quoteSettings.Opp_Stage_Doc_Declined__c;//'Lost';
                        oppTobeUpdated.Won_Lost_Reason__c='No Reason Given'; 
                    }  
                    oppTobeUpdated.Purchase_Order__c=POnumber;
                    update oppTobeUpdated;
                    system.debug('oppTobeUpdated##'+oppTobeUpdated);
                }
            }
        }
        catch(Exception ex){
            ExceptionLogData.InsertExceptionlog(docId, LoggingLevel.ERROR, 'DocVerifyWebhook_Controller', ex, true, false, null, null);
        }
        return null;
    }
    
    public String parameterDetails {
        get {
            
            if(parameterDetails == null) {
                parameterDetails = '';
                
                if (ApexPages.currentPage() != null){
                    
                    Map<String, String> formData = ApexPages.currentPage().getParameters();
                    for (string key : formData.keySet()){
                        if (formData.get(key) != null){
                            parameterDetails += key + ' = ' + formData.get(key) + '\n';
                        }
                    }
                    
                    
                }
            }
            return parameterDetails;
        }
        private set;
    }
    
}