@IsTest
public with sharing class FullCalendarController_Test {

    static testMethod void getEventsTest() {
    
    // Create Webinar Schedules for test
        List < Webinar_Schedule__c> webinarSchedules = new List <Webinar_Schedule__c>();
        for(integer i=0; i<5;i++) {
        
          
          datetime starttime = Datetime.Now().addHours(i);
          datetime endtime =  starttime.addHours(i);
          Webinar_Schedule__c ws = new Webinar_Schedule__c (Name = 'webinar'+i, Start_Time__c = starttime, End_Time__c=endtime);
          webinarSchedules.add(ws);
        
        }
        
        insert webinarSchedules;
        
        Test.StartTest();
        List<Webinar_Schedule__c>events = FullCalendarController.getEvents();
        Test.StopTest();
        
        System.assertEquals(events.size(),5);
    
    
    
    }

}