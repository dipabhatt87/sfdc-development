@isTest
private class LeadConvert_Test {
    
    public DuplicateContactDetailsCompController myComponentController  {get;set;}
    public DuplicateAPContactDetailsCompController apContactController{get;set;}
    public leadConvertDetailsComponentController  leadDetailsComponentController{get;set;}
    public leadConvertTaskInfoComponentController leadTaskController{get;set;}
    
    @testSetup static void setup()
    {
        FakeObjectFactory.testclasssetup();
    }
    
    /*@isTest static void test_method_one() {
LeadService service = new LeadService();
LeadFactory factory = new LeadFactory();
ResponseModel statusModal = new ResponseModel();
statusModal = LeadService.statusModal;
Id leadId;
statusModal = LeadService.createLead('test',8,2,123,878,'firstname','lastname','abc@test.com','job','Accounts Payable','AUS','657','Mathletics','street1','street2','suburb','campaign','accPayFName','accPaLName','abd@test.com','5454df','ihreui84748','enquiry');  
Lead lead = [Select Id,Name from lead where Id =:statusModal.ObjectId ];

}*/
    
    static testMethod void leadConvertTestSuccess() {
        Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;
        
        Contact contact  = FakeObjectFactory.GetContact();        
        TestUtility.AssignAccountToContact(newAccount, contact);        
        insert contact;
        
        Lead newLead = FakeObjectFactory.GetLead();  
        newLead.Account_Payable_First_Name__c = newlead.Firstname;
        newLead.Account_Payable_Last_Name__c = newlead.Lastname;
        newLead.Account_Payable_Email__c = newlead.Email;
        Insert newLead;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(newLead);
        leadConvertController leadController = new leadConvertController(stdController);
        
        //Following code is to test DuplicateContactCompController and DuplicateAPContactCompController
        DuplicateAPContactDetailsCompController apContactController = new DuplicateAPContactDetailsCompController();
        apContactController.leadConvert = newlead;
        apContactController.getContacts();
        ApexPages.currentPage().getParameters().put('APcontactId', contact.Id);
        apContactController.getAPSelected();
        
        DuplicateContactDetailsCompController contactController = new DuplicateContactDetailsCompController();
        contactController.leadConvert = newlead;
        contactController.getContacts();
        ApexPages.currentPage().getParameters().put('contactId', contact.Id);
        contactController.getSelected();
        
        
        leadController.leadConvert = newLead;
        leadController.getMyComponentController();
        leadController.getAPContactComponentController();
        leadController.getLeadDetailsComponentController();
        leadController.getLeadTaskInfoCompController();        
        //leadController.Submit();
        leadController.Cancel();        
        
        PageControllerBase pgBase = new PageControllerBase();
        pgBase.getMyComponentController();
        pgBase.getAPContactComponentController();
        pgBase.getLeadDetailsComponentController();
        pgBase.getLeadTaskInfoCompController();
        pgBase.getThis();
        
        
        ComponentControllerBase compBase = new ComponentControllerBase();
        compBase.pageController = pgBase;
        compBase.pageControllerForAp = pgBase;
        compBase.pageControllerLeadDetails = pgBase;
        compBase.pageControllerLeadTaskInfo = pgBase;        
        
        leadController.setComponentController(new DuplicateContactDetailsCompController());
        leadController.setAPContactComponentController(new DuplicateAPContactDetailsCompController());
        leadController.setLeadDetailsComponentController(new leadConvertDetailsComponentController());
        leadController.setLeadTaskInfoCompController(new leadConvertTaskInfoComponentController());
        /*leadController.setDescriptionComponentController(new leadConvertTaskDescComponentController());
leadController.setTaskComponentController(new leadConvertTaskInfoComponentController() );*/
        
        system.assert(leadController.leadTaskController != null);
        leadController.leadTaskController.taskID.Subject = 'TEST TASK';
        leadController.leadTaskController.taskID.Priority = 'High';
        leadController.leadTaskController.taskID.Status = 'Not Started';
        //leadController.myComponentController.selectedAccount = newAccount.Id;
        
        
        
        leadController.myComponentController.leadConvert = newLead;
        leadController.leadTaskController.leadConvert = newLead;
        leadController.leadDetailsComponentController.leadConvert = newLead;
        Account acc =   leadController.leadDetailsComponentController.getExistingAccount(newAccount.Id);
        System.assertEquals(acc.Id,newAccount.Id);
        leadController.leadDetailsComponentController.selectedAccount = newAccount.Id;
        leadController.leadDetailsComponentController.accountChanged();
        
        leadController.leadDetailsComponentController.selectedAccount = 'NEW';
        
        leadController.leadDetailsComponentController.accountChanged();
        leadController.leadDetailsComponentController.contactID = contact;
        leadController.leadDetailsComponentController.accountLookedUp();
        Contact contact2  = leadController.leadDetailsComponentController.contactID;
        
        System.debug('myComponentController : ' + leadController.myComponentController);
        System.debug('leadDetailsComponentController : ' + leadController.leadDetailsComponentController);
        System.debug('leadTaskController : ' + leadController.leadTaskController);        
        
        //leadController.leadDetailsComponentController.contactID.OwnerId = TestUtility.getFakeId(User.SObjectType);
        // Contact contactID = leadController.leadDetailsComponentController.contactID;
        leadController.leadDetailsComponentController.doNotCreateOppty = true;
        List<SelectOption> leadStatuses = leadController.leadDetailsComponentController.LeadStatusOption;
        
        Opportunity opportunityID = leadController.leadDetailsComponentController.opportunityID;
        //leadController.reminder = true;
        String reminderTime = leadController.leadTaskController.remCon.reminderTime;
        List<SelectOption> timeOptions = leadController.leadTaskController.remCon.ReminderTimeOption;
        
        //ReminderAddMeridiem
        //leadController.leadDetailsComponentController.sendNotificationEmail = true;
        leadController.leadDetailsComponentController.sendOwnerEmail = true;
        string reminderDate = leadController.leadTaskController.remCon.disabledActivityDate;
        
        List<SelectOption> priorityOptions = leadController.leadTaskController.TaskPriorityOption;
        List<SelectOption> statusOptions = leadController.leadTaskController.TaskStatusOption;
        
        
        leadController.myComponentController.createNewContact = true;
        leadController.apContactController.createNewAPContact = true;
        leadController.leadDetailsComponentController.selectedAccount = newAccount.Id; 
        leadController.leadDetailsComponentController.contactId = new Contact();
        leadController.leadDetailsComponentController.contactId.ownerId = UserInfo.getUserId();       
        leadController.Submit(); 
        leadController.PrintErrors(new List<Database.Error>());
        leadController.PrintError('Test');
        
        leadController.myComponentController.createNewContact = false;
        leadController.apContactController.createNewAPContact = true;
        leadController.leadDetailsComponentController.selectedAccount = newAccount.Id;
        //leadController.leadDetailsComponentController.findCompany(newAccount.Name); 
        leadController.leadDetailsComponentController.contactId = new Contact();
        leadController.leadDetailsComponentController.contactId.ownerId = UserInfo.getUserId(); 
        
        leadController.Submit();
        
        leadController.myComponentController.createNewContact = true;
        leadController.apContactController.createNewAPContact = false;
        leadController.leadDetailsComponentController.selectedAccount = newAccount.Id; 
        leadController.leadDetailsComponentController.contactId = new Contact();
        leadController.leadDetailsComponentController.contactId.ownerId = UserInfo.getUserId();       
        leadController.Submit();
        
        leadController.myComponentController.createNewContact = true;
        leadController.apContactController.createNewAPContact = true;
        leadController.leadDetailsComponentController.selectedAccount = 'New'; 
        leadController.leadDetailsComponentController.contactId = new Contact();
        leadController.leadDetailsComponentController.contactId.ownerId = UserInfo.getUserId();       
        leadController.Submit();
        
        leadController.myComponentController.createNewContact = false;
        leadController.apContactController.createNewAPContact = false;
        leadController.leadDetailsComponentController.selectedAccount = 'New'; 
        leadController.leadDetailsComponentController.contactId = new Contact();
        leadController.leadDetailsComponentController.contactId.ownerId = UserInfo.getUserId();       
        leadController.Submit(); 
        
        leadController.myComponentController.createNewContact = false;
        leadController.apContactController.createNewAPContact = false;
        leadController.leadDetailsComponentController.selectedAccount = 'None'; 
        //leadController.leadDetailsComponentController.accounts = null;
        leadController.leadDetailsComponentController.contactId = new Contact();
        leadController.leadDetailsComponentController.contactId.ownerId = UserInfo.getUserId(); 
        leadController.leadConvert.Company = '3P' ;
        update   leadController.leadConvert;
        leadController.leadDetailsComponentController.accounts = null;
        leadController.Submit();
        
        
        
        //see if the new account was created
        /*Account [] checkAccount = [SELECT Id FROM Account WHERE Name ='Test Account' ];
system.debug(checkAccount);
system.assertEquals(1, checkAccount.size(), 'There was a problem converting lead to an account');

//see if the new account was created
Contact [] checkContact = [SELECT Id FROM Contact WHERE Name ='Test Lead' ];
system.debug(checkContact);
system.assertEquals(1, checkContact.size(), 'There was a problem converting lead to a contact');

//
string reminderDate = leadController.myTaskComponentController.remCon.disabledActivityDate;
leadController.myComponentController.accountChanged();

leadController.myComponentController.selectedAccount = 'NEW';

leadController.myComponentController.accountChanged();
*/
        // test the reminder time as a French user to test the 24 hour clock
        Profile p = [select id from profile where name='Standard User'];
        
        User u = new User(alias = 'standt1', email='standarduser1@testorg.com',
                          
                          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                          
                          localesidkey='fr', profileid = p.Id,
                          
                          timezonesidkey='America/Los_Angeles', 
                          username='testUser1@testleadconvert.com');
        
        System.runAs(u) {
            System.debug('DateTimeUtility.LocaleToTimeFormatMap().get(UserInfo.getLocale())' + DateTimeUtility.LocaleToTimeFormatMap().get(UserInfo.getLocale()));
            timeOptions = leadController.leadTaskController.remCon.ReminderTimeOption;
            leadConvertTaskRemindComponentController controller = new leadConvertTaskRemindComponentController();
            controller.ReminderAddMeridiem(timeOptions,'AM');
        }
        
        
        leadController.leadDetailsComponentController.selectedAccount = 'NONE';
        //leadController.leadTaskController.sendNotificationEmail = false;
        
        //test the situation where there is a due date but no subject
        leadController.leadTaskController.taskID.ActivityDate = system.today();
        leadController.leadTaskController.taskID.Subject = '';
        //leadController.leadTaskController.leadConvert.Status = 'NONE';
        
        System.debug('reminder time' + reminderTime);
        LeadConversionHelper helper = new LeadConversionHelper();        
        helper.getContactForLead(contact.Id);        
        // helper.convertToDatetime(Date.today(),reminderTime);
        //convert the lead
        leadController.Submit();
        //leadController.convertLead();
        
        //leadController.myComponentController.accountLookedUp();
        leadController.leadTaskController.DueDateChanged();
        
        
        
        
        
    } 
    
    
    @IsTest(seeAllData = false)
    static void leadConversion_DoNotCreateOpportunityIsUnticked_ConfirmOpportunityIsCreated()
    {
        //Test if system creates a contact and an opportunity when lead is converted and Do not Create 
        //is unticked.
        Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;
        
        Lead newLead = FakeObjectFactory.GetLead();  
        //overwriting the first name
        newlead.Firstname = 'Test';
        newlead.Lastname = 'For Opportunity1';
        newLead.Account_Payable_First_Name__c = newlead.Firstname;
        newLead.Account_Payable_Last_Name__c = newlead.Lastname;
        newLead.Account_Payable_Email__c = newlead.Email;
        Insert newLead;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(newLead);
        leadConvertController leadController = new leadConvertController(stdController);
        leadController.setComponentController(new DuplicateContactDetailsCompController());
        leadController.setAPContactComponentController(new DuplicateAPContactDetailsCompController());
        leadController.setLeadDetailsComponentController(new leadConvertDetailsComponentController());
        leadController.setLeadTaskInfoCompController(new leadConvertTaskInfoComponentController());
        leadController.leadDetailsComponentController.leadConvert = newLead;
        leadController.leadDetailsComponentController.selectedAccount = 'NEW';
        leadController.leadDetailsComponentController.opportunityID = new Opportunity(name = 'Test Opportunity1');
        leadController.leadDetailsComponentController.sendOwnerEmail = false;
        
        leadController.myComponentController.createNewContact = true;
        leadController.apContactController.createNewAPContact = true;
        leadController.leadDetailsComponentController.selectedAccount = newAccount.Id; 
        leadController.leadDetailsComponentController.jobFunction ='Teacher';
        
        leadController.Submit();
        List<Contact> contacts = [Select id, firstname, lastname from contact where firstname = 'Test' and 
                                  lastname = 'For Opportunity1' and accountid = :newAccount.id ];
        System.assert(!contacts.isEmpty() && contacts.size() == 1 , 'No contact created.');
        List<Opportunity> oppos = [Select id, name from Opportunity where name = 'Test Opportunity1' and accountid = :newAccount.id ];
        System.assert(!oppos.isEmpty() && oppos.size() == 1, 'No Opportunity is created.');     
    }
    
    @IsTest(seeAllData = false)
    static void leadConversion_DoNotCreateOpportunityIsTicked_NoOpportunityIsCreated()
    {
        //Test if system creates a contact but no  opportunity is created when lead is converted and Do not Create 
        //is ticked.
        Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;
        
        Lead newLead = FakeObjectFactory.GetLead();  
        //overwriting the first name
        newlead.Firstname = 'Test';
        newlead.Lastname = 'For Opportunity2';
        newLead.Account_Payable_First_Name__c = newlead.Firstname;
        newLead.Account_Payable_Last_Name__c = newlead.Lastname;
        newLead.Account_Payable_Email__c = newlead.Email;
        Insert newLead;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(newLead);
        leadConvertController leadController = new leadConvertController(stdController);
        leadController.setComponentController(new DuplicateContactDetailsCompController());
        leadController.setAPContactComponentController(new DuplicateAPContactDetailsCompController());
        leadController.setLeadDetailsComponentController(new leadConvertDetailsComponentController());
        leadController.setLeadTaskInfoCompController(new leadConvertTaskInfoComponentController());
        leadController.leadDetailsComponentController.leadConvert = newLead;
        leadController.leadDetailsComponentController.selectedAccount = 'NEW';
        leadController.leadDetailsComponentController.opportunityID = new Opportunity(name = 'Test Opportunity2');
        leadController.leadDetailsComponentController.sendOwnerEmail = false;
        leadController.leadDetailsComponentController.doNotCreateOppty = true;
        
        leadController.myComponentController.createNewContact = true;
        leadController.apContactController.createNewAPContact = true;
        leadController.leadDetailsComponentController.selectedAccount = newAccount.Id; 
        leadController.leadDetailsComponentController.jobFunction ='Teacher';
        
        leadController.Submit();
        List<Contact> contcts = [Select id, firstname, lastname from contact where firstname = 'Test' and 
                                 lastname = 'For Opportunity2' and accountid = :newAccount.id ];
        System.assert(!contcts.isEmpty() && contcts.size() == 1, 'No contact created.');
        List<Opportunity> oppos = [Select id, name from Opportunity where name = 'Test Opportunity2' and accountid = :newAccount.id ];
        System.assert(oppos.isEmpty(), 'No Opportunity should be created');    
    }
    
    @IsTest(seeAllData = false)
    static void leadConversion_DoNotCreateOpportunityIsUnticked_ConfirmContactRoleCreated()
    {
        ISOMapping__c iso=FakeObjectFactory.GetIsoMap();
        iso.Territory__c='APAC';
        iso.Name ='Australia';
        insert iso;
        
        Account newAccount  = FakeObjectFactory.GetSchoolAccount();
        Insert newAccount;
        
        Contact newContact= FakeObjectFactory.GetContact();
        newContact.AccountId = newAccount.id;
        insert newContact;
        
        Lead newLead = FakeObjectFactory.GetLead();  
        //overwriting the first name
        newlead.Firstname = 'Test';
        newlead.Lastname = 'For Opportunity1';
        newLead.Country ='Australia';
        Insert newLead;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(newLead);
        leadConvertController leadController = new leadConvertController(stdController);
        leadController.setComponentController(new DuplicateContactDetailsCompController());
        leadController.setAPContactComponentController(new DuplicateAPContactDetailsCompController());
        leadController.setLeadDetailsComponentController(new leadConvertDetailsComponentController());
        leadController.setLeadTaskInfoCompController(new leadConvertTaskInfoComponentController());
        leadController.leadDetailsComponentController.leadConvert = newLead;
        leadController.leadDetailsComponentController.sendOwnerEmail = false;
        
        leadController.myComponentController.createNewContact = true;
        leadController.apContactController.createNewAPContact = true;
        leadController.myComponentController.selectedcontactId= newContact.Id;
        leadController.leadDetailsComponentController.selectedAccount = newAccount.Id; 
        leadController.leadDetailsComponentController.doNotCreateOppty= true;
        leadController.leadDetailsComponentController.jobFunction ='Teacher';
        
        leadController.Submit();
        //List<Contact> contacts = [Select id, firstname, lastname from contact where firstname = 'Test' and 
        //lastname = 'For Opportunity1' and accountid = :newAccount.id ];
        //System.assert(!contacts.isEmpty() && contacts.size() == 1 , 'No contact created.');
        List<contact_Roles__c> contRoles = [Select id, job_function__c from contact_Roles__c ];
        system.debug('contRoles##'+ contRoles);
        System.assert(contRoles.size()>0);     
    }
    
}