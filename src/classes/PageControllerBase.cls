public with sharing virtual class PageControllerBase {
	public PageControllerBase() {
		
	}

private ComponentControllerBase myComponentController;
private ComponentControllerBase apContactComponentController;
private ComponentControllerBase	leadDetailsComponentController;
private ComponentControllerBase	leadTaskInfoCompController;
	
  public virtual ComponentControllerBase getMyComponentController() {
    return myComponentController;
  }

  public virtual void setComponentController(ComponentControllerBase compController) {
    myComponentController = compController;
  }

  public virtual ComponentControllerBase getAPContactComponentController() {
    return apContactComponentController;
  }

  public virtual void setAPContactComponentController(ComponentControllerBase compController) {
    apContactComponentController = compController;
  }

   public virtual ComponentControllerBase getLeadDetailsComponentController() {
    return leadDetailsComponentController;
  }

  public virtual void setLeadDetailsComponentController(ComponentControllerBase compController) {
    leadDetailsComponentController = compController;
  }

  public virtual ComponentControllerBase getLeadTaskInfoCompController() {
    return leadTaskInfoCompController;
  }

  public virtual void setLeadTaskInfoCompController(ComponentControllerBase compController) {
    leadTaskInfoCompController = compController;
  }
	
  public PageControllerBase getThis() {
    return this;
  }
}