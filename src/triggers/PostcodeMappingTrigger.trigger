trigger PostcodeMappingTrigger on Postcode_Mapping__c (after insert, after update, before insert, before update) {
    if(!Trigger_Handler__c.getInstance('Block Trigger').Block_Trigger_Execution__c) {
       TriggerFactory.createTriggerDispatcher(Postcode_Mapping__c.sObjectType);
   }
}