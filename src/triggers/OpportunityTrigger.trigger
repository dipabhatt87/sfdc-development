trigger OpportunityTrigger on Opportunity (after insert, after update, before insert, before update, before delete)  {
    TriggerFactory.createTriggerDispatcher(Opportunity.sObjectType);
}