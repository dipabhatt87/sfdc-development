trigger OpportunityLineItemTrigger on OpportunityLineItem (before delete,after insert, after update, before insert, before update) {
 TriggerFactory.createTriggerDispatcher(OpportunityLineItem.sObjectType);
}