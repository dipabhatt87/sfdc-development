trigger Product2Before on Product2 (before insert, before update) {
     for(Product2 p : Trigger.new) {
        if(p.ACTNSCRM__NetSuite_Internal_ID__c != null && p.ACTNSCRM__NetSuite_Internal_ID__c != '') {
            p.ACTNSPC__External_Id__c = p.ACTNSCRM__NetSuite_Internal_ID__c;
        }
    }
}