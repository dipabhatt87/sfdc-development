<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>3P Sales</label>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>standard-Chatter</tab>
    <tab>Licences_Renewal</tab>
    <tab>EBS_Responses__c</tab>
    <tab>standard-Case</tab>
    <tab>Price_Calculator</tab>
</CustomApplication>
