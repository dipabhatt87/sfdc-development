<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>General_Exception_Developer_Email_Alert</fullName>
        <description>General Exception Developer Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Developers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Exception_Developer_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>General_Exception_User_Email_Alert</fullName>
        <description>General Exception User Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/General_Exception_User_Email_Template</template>
    </alerts>
</Workflow>
