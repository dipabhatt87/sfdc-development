<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Cap_Email_Alert</fullName>
        <description>Cap Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Licence_Cap_Breach</template>
    </alerts>
    <fieldUpdates>
        <fullName>Increment_Trial_Licence</fullName>
        <description>Add 1 to Trial Licence count</description>
        <field>Trial_Licences__c</field>
        <formula>IF(ISBLANK(Trial_Licences__c),1,  Trial_Licences__c + 1)</formula>
        <name>Increment Trial Licence</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetAvoidTriggerExecution</fullName>
        <field>AvoidTriggerExecution__c</field>
        <formula>(AvoidTriggerExecution__c + 1)</formula>
        <name>SetAvoidTriggerExecution</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetAvoidTriggerExecutionVF</fullName>
        <field>AvoidTriggerExecution__c</field>
        <formula>(AvoidTriggerExecution__c + 1)</formula>
        <name>SetAvoidTriggerExecutionVF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Original_Licence_End_Date</fullName>
        <field>Original_End_Date__c</field>
        <formula>IF(ISBLANK(PRIORVALUE(UsageEndDate)),UsageEndDate,PRIORVALUE(UsageEndDate))</formula>
        <name>Set Original Licence End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Trigger_Flag</fullName>
        <field>Set_Customer_Status__c</field>
        <literalValue>1</literalValue>
        <name>Set Trigger Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Licence_End_Date</fullName>
        <field>UsageEndDate</field>
        <formula>TODAY()</formula>
        <name>Update Licence End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cap Breach</fullName>
        <actions>
            <name>Cap_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Trigger Email on Breach of Licence cap</description>
        <formula>AND(  
Registered_Students__c &lt;&gt; PRIORVALUE(Registered_Students__c),
Registered_Students__c &gt; 0,
License_Cap__c &gt; 0,
(Registered_Students__c / License_Cap__c )&gt; 1.05 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Increment Trial</fullName>
        <actions>
            <name>Increment_Trial_Licence</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Add 1 to Counter of Trial Licences when Licence expiry date changed</description>
        <formula>AND(ISCHANGED( UsageEndDate ),  RecordType.Name = &quot;Trial Licence&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Licence End Data to Today</fullName>
        <actions>
            <name>Update_Licence_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.UsageEndDate</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Update Licence End Data to Today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Original Licence End Date</fullName>
        <actions>
            <name>SetAvoidTriggerExecutionVF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Original_Licence_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(Original_End_Date__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set customer status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>notEqual</operation>
            <value>Existing Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Provisioned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.RecordTypeId</field>
            <operation>equals</operation>
            <value>Full License</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SetAvoidTriggerExecutionVF</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Trigger_Flag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Asset.InstallDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Renewal_Task</fullName>
        <assignedToType>owner</assignedToType>
        <description>Renewal Task will automatically be set-up 45 days prior to the expiry date of the asset. It will be assigned to the Asset owner - This excludes New Zealand</description>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Renewal Task</subject>
    </tasks>
    <tasks>
        <fullName>Renewal_Tasks</fullName>
        <assignedToType>owner</assignedToType>
        <description>Renewal Task 45 days prior to Asset expiry date - Excludes New Zealand</description>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Renewal Task</subject>
    </tasks>
</Workflow>
