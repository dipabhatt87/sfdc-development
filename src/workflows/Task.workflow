<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Sub_type_field_update_End</fullName>
        <description>Update Syb-type when automated trial follow-up task creation</description>
        <field>Activity_Subtype__c</field>
        <literalValue>Trial: End-trial review</literalValue>
        <name>Sub-type field update End</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Subtype_field_update</fullName>
        <description>Will update the Activity subtype on automatic creation of trial follow-up tasks</description>
        <field>Activity_Subtype__c</field>
        <literalValue>Trial: Mid-trial review</literalValue>
        <name>Subtype field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Activity_Type_Sub_Type</fullName>
        <field>Activity_Type__c</field>
        <literalValue>Email</literalValue>
        <name>Update Activity Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SubType</fullName>
        <field>Activity_Subtype__c</field>
        <literalValue>Other</literalValue>
        <name>Update SubType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Activity Type Sub Type</fullName>
        <actions>
            <name>Update_Activity_Type_Sub_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SubType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Activity_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Activity_Subtype__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Email:</value>
        </criteriaItems>
        <description>Update Activity Type Sub Type for Email Record</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
