<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_to_event_Assigned_To_person</fullName>
        <description>Send Email to event Assigned To person</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Event_Assigned</template>
    </alerts>
    <rules>
        <fullName>Send Email Invitations</fullName>
        <actions>
            <name>Send_Email_to_event_Assigned_To_person</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$User.Id &lt;&gt; OwnerId</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
