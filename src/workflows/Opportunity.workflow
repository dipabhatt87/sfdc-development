<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EME_Onboarding</fullName>
        <ccEmails>support@3plearning.co.uk</ccEmails>
        <description>EME Onboarding</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Existing_Client_templates/EME_OnBoarding_2</template>
    </alerts>
    <alerts>
        <fullName>EME_Onboarding_for_Middle_East</fullName>
        <ccEmails>sara.khan@3plearning.com</ccEmails>
        <description>EME Onboarding for Middle East</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Existing_Client_templates/EME_OnBoarding_2</template>
    </alerts>
    <alerts>
        <fullName>New_Opportunity_Created</fullName>
        <description>New Opportunity Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>BDM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>KAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderAddress>premium@3plearning.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Opportunity_Quote_for_Gold_or_Platinum_Schools</template>
    </alerts>
    <alerts>
        <fullName>New_Opportunity_Created_for_Gold_or_Platinum_Schools</fullName>
        <description>New Opportunity Created for Gold or Platinum Schools</description>
        <protected>false</protected>
        <recipients>
            <recipient>BDM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>KAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderAddress>premium@3plearning.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Opportunity_Quote_for_Gold_or_Platinum_Schools</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Sold_Adjustment</fullName>
        <description>Opportunity Sold - Adjustment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Sold_Adjustment</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Sold_or_Invoiced_for_Bronze_or_Silver_Schools</fullName>
        <description>Opportunity Sold or Invoiced for Bronze or Silver Schools</description>
        <protected>false</protected>
        <recipients>
            <recipient>BDM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>KAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderAddress>premium@3plearning.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Opportunity_Sold_or_Invoiced_for_Bronze_or_Silver_Schools</template>
    </alerts>
    <fieldUpdates>
        <fullName>ACTNSCRM__Set_Contract_End_Date</fullName>
        <field>ACTNSCRM__Contract_End_Date__c</field>
        <formula>ACTNSCRM__Contract_Start_Date__c + Floor( ACTNSCRM__Contract_Term__c /12 *365 )</formula>
        <name>Set Contract End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Locked_Record_Type</fullName>
        <description>Locked Opportunity Record Type</description>
        <field>RecordTypeId</field>
        <lookupValue>Locked_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Locked Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Locked_Flag</fullName>
        <description>Record Locked flag update to True</description>
        <field>Record_Locked__c</field>
        <literalValue>1</literalValue>
        <name>Record Locked Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Opportunity_Quote_Ignore_Flag</fullName>
        <description>Reset the Quote_Ignore flag to false when Amount is changed. So that Validation rules can be executed.</description>
        <field>Quote_Ignore__c</field>
        <literalValue>0</literalValue>
        <name>Reset Opportunity Quote Ignore Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sold_Role</fullName>
        <field>Sold_Role__c</field>
        <formula>Owner_Role__c</formula>
        <name>Set Sold Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage</fullName>
        <field>StageName</field>
        <literalValue>Interest</literalValue>
        <name>Set Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unlock_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Standard_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Unlock Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Status</fullName>
        <description>Set Account Status</description>
        <field>Status__c</field>
        <literalValue>Existing customer</literalValue>
        <name>Update Account Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Date</fullName>
        <description>Update Closed Date to Today</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Update Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_To_Sold</fullName>
        <field>StageName</field>
        <literalValue>Sold</literalValue>
        <name>Update Opportunity To Sold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACTNSCRM__Default Contract End Date</fullName>
        <actions>
            <name>ACTNSCRM__Set_Contract_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.ACTNSCRM__Contract_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ACTNSCRM__Contract_End_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ACTNSCRM__Contract_Term__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ACTNSCRM__Order_Type__c</field>
            <operation>equals</operation>
            <value>Contract - New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Date Opty Sold</fullName>
        <actions>
            <name>Update_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <description>Update Close Date to Today when Opportunity Stage is updated to Sold or Sold - Invoiced or Lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create_Email_Alert_For_Quote</fullName>
        <actions>
            <name>New_Opportunity_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Is_Automatically_Created__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Can_Send_Email_Alert__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send an email to KAM/BDM/SDR for Quote.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Create_Task_Email_to_KAM_BDM_For_Gold_Platinum</fullName>
        <actions>
            <name>New_Opportunity_Created_for_Gold_or_Platinum_Schools</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.School_Classification__c</field>
            <operation>equals</operation>
            <value>Gold,Platinum</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Quote</value>
        </criteriaItems>
        <description>Create a email and Task notification to the KAM/BDM for Gold/Platinum</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EME Onboarding</fullName>
        <actions>
            <name>EME_Onboarding</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( 
 OR(TEXT(Account.ShippingCountryCode) = &quot;GB&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;AL&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;AD&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;AM&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;AT&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;AZ&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;BY&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;BE&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;BA&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;BG&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;HR&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;CY&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;CZ&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;DK&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;EE&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;FI&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;FR&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;GE&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;DE&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;GR&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;HU&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;IS&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;IE&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;IT&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;KZ&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;XK&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;LV&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;LI&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;LT&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;LU&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;MK&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;MT&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;MD&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;MC&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;ME&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;NL&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;NO&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;PL&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;PT&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;RO&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;RU&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;SM&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;RS&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;SK&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;SI&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;ES&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;SE&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;CH&quot;, 
	TEXT(Account.ShippingCountryCode) = &quot;UA&quot;), 
OR( TEXT(Type) = &quot;New Business&quot;, TEXT(Type) = &quot;Cross Sell&quot;), 
TEXT(StageName) = &quot;Sold&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EME Onboarding Middle East</fullName>
        <actions>
            <name>EME_Onboarding_for_Middle_East</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is an EME Onboarding for excluded countries from EME Onboarding.</description>
        <formula>AND(   OR(TEXT(Account.ShippingCountryCode) = &quot;AF&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;DZ&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;BH&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;EG&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;IR&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;IQ&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;IL&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;JO&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;KW&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;KG&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;LB&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;LY&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;MA&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;OM&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;PS&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;QA&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;SA&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;SY&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;TJ&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;TN&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;AE&quot;,  	TEXT(Account.ShippingCountryCode) = &quot;UZ&quot;),  OR( TEXT(Type) = &quot;New Business&quot;, TEXT(Type) = &quot;Cross Sell&quot;),  TEXT(StageName) = &quot;Sold&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email_to_KAM_And_BDM_When_Opportunity_Sold_or_Invoiced</fullName>
        <actions>
            <name>Opportunity_Sold_or_Invoiced_for_Bronze_or_Silver_Schools</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.School_Classification__c</field>
            <operation>equals</operation>
            <value>Bronze,Silver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Sold,Sold - Invoiced</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lock Record after 7 Days for Lost Opportunities</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <description>LOST Opportunity will be lock after 7 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Locked_Record_Type</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lock record after invoicing</fullName>
        <actions>
            <name>Locked_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Sold - Invoiced</value>
        </criteriaItems>
        <description>Locking the record once the Sold Opportunity has been invoiced.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity_Quote_Signed_Or_Completed</fullName>
        <actions>
            <name>Update_Opportunity_To_Sold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Is_Docverify_Signed_Or_Completed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reset_Opportunity_Quote_Ignore_Flag</fullName>
        <actions>
            <name>Reset_Opportunity_Quote_Ignore_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reset the Quote_Ignore flag to false when Amount is changed. So that Validation rules can be executed.</description>
        <formula>AND( NOT( ISNEW() ), Quote_Ignore__c = true, Amount  &lt;&gt;  PRIORVALUE(Amount) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Sold Owner Role</fullName>
        <actions>
            <name>Set_Sold_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Sold,Sold - Adjustment,Sold - Invoiced</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sold_Role__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnLock record for Adjustment</fullName>
        <actions>
            <name>Opportunity_Sold_Adjustment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Unlock_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Sold - Adjustment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Locked Opportunity</value>
        </criteriaItems>
        <description>Unlocking the record once the Sold Opportunity is reverted to Sold-Adjustment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Task_to_BDM_For_Bronze_Silver</fullName>
        <assignedTo>BDM</assignedTo>
        <assignedToType>accountTeam</assignedToType>
        <description>Activity type: Online Purchase
Activity Subtype: Purchase
Product: Mathletics
Please complete below tasks within 2 weeks:
Professional Training
Handover the relevant Client Manager</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New online purchase requires client onboarding</subject>
    </tasks>
    <tasks>
        <fullName>Task_to_BDM_for_Gold_or_Platinum</fullName>
        <assignedTo>BDM</assignedTo>
        <assignedToType>accountTeam</assignedToType>
        <description>Activity type: Online Purchase
Activity Subtype: Quote
Product Mathletics
Comments:
This Gold/Platinum school has requested for a quote to purchase Mathletics online.
Please review and identify any up-sell or cross-sell opportunity.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New online purchase requires urgent review</subject>
    </tasks>
    <tasks>
        <fullName>Task_to_KAM_For_Bronze_Silver</fullName>
        <assignedTo>KAM</assignedTo>
        <assignedToType>accountTeam</assignedToType>
        <description>Activity type: Online Purchase
Activity Subtype: Purchase
Product: Mathletics
Please complete below tasks within 2 weeks:
Professional Training
Handover the relevant Client Manager</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New online purchase requires client onboarding</subject>
    </tasks>
    <tasks>
        <fullName>Task_to_KAM_for_Gold_or_Platinum</fullName>
        <assignedToType>owner</assignedToType>
        <description>Activity type: Online Purchase
Activity Subtype: Quote
Product Mathletics
Comments:
This Gold/Platinum school has requested for a quote to purchase Mathletics online.
Please review and identify any up-sell or cross-sell opportunity.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New online purchase requires urgent review</subject>
    </tasks>
</Workflow>
