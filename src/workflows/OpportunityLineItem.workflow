<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Cluster_Discount_Update</fullName>
        <description>Used to update the Discount field on an opportunity product line item based on the Cluster Discount for the product</description>
        <field>Discount</field>
        <formula>Opportunity.Account.Cluster_Discount_Mathletics__c</formula>
        <name>Cluster Discount Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Workflow_Order_to_1</fullName>
        <field>Workflow_Order__c</field>
        <formula>1</formula>
        <name>Set Opp Workflow Order to 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Workflow_Order_to_2</fullName>
        <field>Workflow_Order__c</field>
        <formula>2</formula>
        <name>Set Opp Workflow Order to 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Workflow_Order_to_3</fullName>
        <field>Workflow_Order__c</field>
        <formula>3</formula>
        <name>Set Opp Workflow Order to 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Site_Product_Quantity</fullName>
        <field>Quantity</field>
        <formula>IF( CONTAINS(Product2.Name,&apos;-Site-&apos;),IF( Product2.ACTNSPC__Quantity_Tier_2__c &gt; Quantity, Product2.ACTNSPC__Quantity_Tier_2__c-1,
IF( Product2.ACTNSPC__Quantity_Tier_3__c &gt; Quantity, Product2.ACTNSPC__Quantity_Tier_3__c-1,
IF( Product2.ACTNSPC__Quantity_Tier_4__c &gt; Quantity, Product2.ACTNSPC__Quantity_Tier_4__c-1,
IF( Product2.ACTNSPC__Quantity_Tier_5__c &gt; Quantity, Product2.ACTNSPC__Quantity_Tier_5__c-1,
IF( Product2.Quantity_Tier_6__c &gt; Quantity, Product2.Quantity_Tier_6__c-1,
IF( Product2.Quantity_Tier_7__c &gt; Quantity, Product2.Quantity_Tier_7__c-1,
IF( Product2.Quantity_Tier_8__c &gt; Quantity, Product2.Quantity_Tier_8__c-1,
IF( Product2.Quantity_Tier_9__c &gt; Quantity, Product2.Quantity_Tier_9__c-1,
IF( Product2.Quantity_Tier_10__c &gt; Quantity, Product2.Quantity_Tier_10__c-1,
IF( Product2.Quantity_Tier_11__c &gt; Quantity, Product2.Quantity_Tier_11__c-1,
IF( Product2.Quantity_Tier_12__c &gt; Quantity, Product2.Quantity_Tier_12__c-1,
IF( Product2.Quantity_Tier_13__c &gt; Quantity, Product2.Quantity_Tier_13__c-1,
IF( Product2.Quantity_Tier_14__c &gt; Quantity, Product2.Quantity_Tier_14__c-1,
IF( Product2.Quantity_Tier_15__c &gt; Quantity, Product2.Quantity_Tier_15__c-1,
IF( Product2.Quantity_Tier_16__c &gt; Quantity, Product2.Quantity_Tier_16__c-1,
IF( Product2.Quantity_Tier_17__c &gt; Quantity, Product2.Quantity_Tier_17__c-1,
Quantity)))))))))))))))),
Quantity)</formula>
        <name>Set Opportunity Site Product Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Original_Price</fullName>
        <field>Orginal_Sales_Price__c</field>
        <formula>ListPrice</formula>
        <name>Set Opportunity Original Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Tax_From_Formula</fullName>
        <field>Tax_Set__c</field>
        <formula>Tax_Amt__c</formula>
        <name>Set Tax From Formula</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Family_into_Unique_Field</fullName>
        <field>UniqueOpportunityProduct__c</field>
        <formula>Family__c+Product2Id+OpportunityId</formula>
        <name>Update Product Family into Unique Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Price</fullName>
        <field>UnitPrice</field>
        <formula>IF(CONTAINS(  Product2.Name  , &apos;-Site-&apos;),IF(ISNULL( ACTNSPC__Quantity_Price__c ),Orginal_Sales_Price__c *  Licence_Months__c/  Quantity , ACTNSPC__Quantity_Price__c * Licence_Months__c/  Quantity)
,IF(ISNULL( ACTNSPC__Quantity_Price__c ),Orginal_Sales_Price__c *  Licence_Months__c, ACTNSPC__Quantity_Price__c * Licence_Months__c))</formula>
        <name>Update Sales Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_other_product</fullName>
        <field>Other_Product__c</field>
        <literalValue>1</literalValue>
        <name>Update other product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Opp Line Qty Top of Band</fullName>
        <actions>
            <name>Set_Opp_Workflow_Order_to_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opportunity_Site_Product_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.Workflow_Order__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>AS - Inactivated on 13th Feb 2017 - Moving to Flow for Price Locking on Opportunity Line Item</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opp Original Sales Price</fullName>
        <actions>
            <name>Set_Opp_Workflow_Order_to_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Original_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>OpportunityLineItem.Workflow_Order__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Workflow_Order__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>AS - Inactivated on 13th Feb 2017 - Moving to Flow for Price Locking on Opportunity Line Item</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Other Product flag</fullName>
        <actions>
            <name>Update_other_product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Is_Other_Product__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>based on product family</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Tax</fullName>
        <actions>
            <name>Set_Tax_From_Formula</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opp Sales Price</fullName>
        <actions>
            <name>Set_Opp_Workflow_Order_to_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Sales_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>AS - Inactivated on 13th Feb 2017 - Moving to Flow for Price Locking on Opportunity Line Item</description>
        <formula>AND(OR ( AND( NOT(ISNULL(SubscriptionStartDate__c)),  NOT(ISNULL(SubscriptionEndDate__c)) ), ISCHANGED(SubscriptionStartDate__c),  ISCHANGED(SubscriptionEndDate__c)  ) ,  Workflow_Order__c = 2)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Unique Field</fullName>
        <actions>
            <name>Update_Product_Family_into_Unique_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Provision_Full_License__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
