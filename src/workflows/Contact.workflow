<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Account_Owner</fullName>
        <description>Notify Account Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Principal_Dept_Head_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Reference_Contact</fullName>
        <description>Set the Contact Reference Field</description>
        <field>Reference_Contact__c</field>
        <literalValue>1</literalValue>
        <name>Set Reference Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Principal Notification</fullName>
        <actions>
            <name>Notify_Account_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends email when a Head is Added as a Contact on the School</description>
        <formula>AND( 
OR( 
TEXT(Job_Function__c) = &apos;Head of Department&apos;, 
TEXT(Job_Function__c) = &apos;Head of Senior School&apos;, 
TEXT(Job_Function__c) = &apos;Head of Junior School&apos;, 
TEXT(Job_Function__c) = &apos;HOD&apos;, 
TEXT(Job_Function__c) = &apos;Executive Headteacher&apos;, 
TEXT(Job_Function__c) = &apos;Headteacher&apos;, 
TEXT(Job_Function__c) = &apos;Head of Phase&apos;, 
TEXT(Job_Function__c) = &apos;Head of Phase – Upper&apos;, 
TEXT(Job_Function__c) = &apos;Head of Phase – Lower&apos;, 
TEXT(Job_Function__c) = &apos;Head of Phase – Middle&apos;, 
TEXT(Job_Function__c) = &apos;Head of Phase – Early Ed&apos;, 
TEXT(Job_Function__c) = &apos;Department Head&apos; 
), 
$User.Id &lt;&gt; Account.Owner.Id 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Reference Contact</fullName>
        <actions>
            <name>Set_Reference_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check the Reference contact if any of the product flags are set</description>
        <formula>AND(   OR(Reference_IntoScience__c = true,  Reference_Mathletics__c = true , Reference_ReadingEggs__c = true, Reference_Spellodrome__c = true),  Reference_Contact__c = false  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
